﻿namespace OthelloAI
{
	partial class GameView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameView));
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.BoardImage = new System.Windows.Forms.PictureBox();
			this.GameInfoLayout = new System.Windows.Forms.TableLayoutPanel();
			this.InfoTitle = new System.Windows.Forms.Label();
			this.Player1Label = new System.Windows.Forms.Label();
			this.Player2Label = new System.Windows.Forms.Label();
			this.TurnLabel = new System.Windows.Forms.Label();
			this.TimerLabel = new System.Windows.Forms.Label();
			this.GameTimerText = new System.Windows.Forms.Label();
			this.Player1Details = new System.Windows.Forms.Label();
			this.Player1Piece = new System.Windows.Forms.PictureBox();
			this.Player2Piece = new System.Windows.Forms.PictureBox();
			this.Player2Details = new System.Windows.Forms.Label();
			this.ServerStatusText = new System.Windows.Forms.Label();
			this.ConsoleOutput = new System.Windows.Forms.ListBox();
			this.GameWorker = new System.ComponentModel.BackgroundWorker();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.BoardImage)).BeginInit();
			this.GameInfoLayout.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Player1Piece)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Player2Piece)).BeginInit();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.Controls.Add(this.BoardImage, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.GameInfoLayout, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.ConsoleOutput, 0, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(546, 514);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// BoardImage
			// 
			this.BoardImage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BoardImage.Location = new System.Drawing.Point(3, 3);
			this.BoardImage.Name = "BoardImage";
			this.BoardImage.Size = new System.Drawing.Size(267, 405);
			this.BoardImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.BoardImage.TabIndex = 1;
			this.BoardImage.TabStop = false;
			this.BoardImage.SizeChanged += new System.EventHandler(this.BoardImage_SizeChanged);
			this.BoardImage.Click += new System.EventHandler(this.BoardImage_Click);
			// 
			// GameInfoLayout
			// 
			this.GameInfoLayout.ColumnCount = 4;
			this.GameInfoLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.GameInfoLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.GameInfoLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.GameInfoLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.GameInfoLayout.Controls.Add(this.InfoTitle, 0, 0);
			this.GameInfoLayout.Controls.Add(this.Player1Label, 0, 1);
			this.GameInfoLayout.Controls.Add(this.Player2Label, 0, 2);
			this.GameInfoLayout.Controls.Add(this.TurnLabel, 0, 4);
			this.GameInfoLayout.Controls.Add(this.TimerLabel, 2, 3);
			this.GameInfoLayout.Controls.Add(this.GameTimerText, 3, 3);
			this.GameInfoLayout.Controls.Add(this.Player1Details, 2, 1);
			this.GameInfoLayout.Controls.Add(this.Player1Piece, 3, 1);
			this.GameInfoLayout.Controls.Add(this.Player2Piece, 3, 2);
			this.GameInfoLayout.Controls.Add(this.Player2Details, 2, 2);
			this.GameInfoLayout.Controls.Add(this.ServerStatusText, 0, 5);
			this.GameInfoLayout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GameInfoLayout.Location = new System.Drawing.Point(276, 3);
			this.GameInfoLayout.Name = "GameInfoLayout";
			this.GameInfoLayout.RowCount = 7;
			this.GameInfoLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.GameInfoLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.GameInfoLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.GameInfoLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.GameInfoLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.GameInfoLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.GameInfoLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.GameInfoLayout.Size = new System.Drawing.Size(267, 405);
			this.GameInfoLayout.TabIndex = 2;
			// 
			// InfoTitle
			// 
			this.InfoTitle.AutoSize = true;
			this.GameInfoLayout.SetColumnSpan(this.InfoTitle, 4);
			this.InfoTitle.Dock = System.Windows.Forms.DockStyle.Fill;
			this.InfoTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.InfoTitle.Location = new System.Drawing.Point(3, 0);
			this.InfoTitle.Name = "InfoTitle";
			this.InfoTitle.Size = new System.Drawing.Size(261, 50);
			this.InfoTitle.TabIndex = 0;
			this.InfoTitle.Text = "Reversi Game Info";
			this.InfoTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Player1Label
			// 
			this.Player1Label.AutoSize = true;
			this.GameInfoLayout.SetColumnSpan(this.Player1Label, 2);
			this.Player1Label.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Player1Label.Location = new System.Drawing.Point(3, 50);
			this.Player1Label.Name = "Player1Label";
			this.Player1Label.Size = new System.Drawing.Size(126, 50);
			this.Player1Label.TabIndex = 1;
			this.Player1Label.Text = "Player 1";
			this.Player1Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Player2Label
			// 
			this.Player2Label.AutoSize = true;
			this.GameInfoLayout.SetColumnSpan(this.Player2Label, 2);
			this.Player2Label.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Player2Label.Location = new System.Drawing.Point(3, 100);
			this.Player2Label.Name = "Player2Label";
			this.Player2Label.Size = new System.Drawing.Size(126, 50);
			this.Player2Label.TabIndex = 2;
			this.Player2Label.Text = "Player 2";
			this.Player2Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// TurnLabel
			// 
			this.TurnLabel.AutoSize = true;
			this.GameInfoLayout.SetColumnSpan(this.TurnLabel, 4);
			this.TurnLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TurnLabel.Location = new System.Drawing.Point(3, 200);
			this.TurnLabel.Name = "TurnLabel";
			this.TurnLabel.Size = new System.Drawing.Size(261, 50);
			this.TurnLabel.TabIndex = 3;
			this.TurnLabel.Text = "Turn Information";
			this.TurnLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// TimerLabel
			// 
			this.TimerLabel.AutoSize = true;
			this.TimerLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TimerLabel.Location = new System.Drawing.Point(135, 150);
			this.TimerLabel.Name = "TimerLabel";
			this.TimerLabel.Size = new System.Drawing.Size(60, 50);
			this.TimerLabel.TabIndex = 4;
			this.TimerLabel.Text = "Game Timer";
			this.TimerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// GameTimerText
			// 
			this.GameTimerText.AutoSize = true;
			this.GameTimerText.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GameTimerText.Location = new System.Drawing.Point(201, 150);
			this.GameTimerText.Name = "GameTimerText";
			this.GameTimerText.Size = new System.Drawing.Size(63, 50);
			this.GameTimerText.TabIndex = 7;
			this.GameTimerText.Text = "00:00";
			this.GameTimerText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Player1Details
			// 
			this.Player1Details.AutoSize = true;
			this.Player1Details.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Player1Details.Location = new System.Drawing.Point(135, 50);
			this.Player1Details.Name = "Player1Details";
			this.Player1Details.Size = new System.Drawing.Size(60, 50);
			this.Player1Details.TabIndex = 8;
			this.Player1Details.Text = "label2";
			this.Player1Details.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Player1Piece
			// 
			this.Player1Piece.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Player1Piece.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Player1Piece.ErrorImage = null;
			this.Player1Piece.Location = new System.Drawing.Point(201, 53);
			this.Player1Piece.Name = "Player1Piece";
			this.Player1Piece.Size = new System.Drawing.Size(63, 44);
			this.Player1Piece.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.Player1Piece.TabIndex = 9;
			this.Player1Piece.TabStop = false;
			// 
			// Player2Piece
			// 
			this.Player2Piece.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Player2Piece.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Player2Piece.ErrorImage = null;
			this.Player2Piece.Location = new System.Drawing.Point(201, 103);
			this.Player2Piece.Name = "Player2Piece";
			this.Player2Piece.Size = new System.Drawing.Size(63, 44);
			this.Player2Piece.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
			this.Player2Piece.TabIndex = 10;
			this.Player2Piece.TabStop = false;
			// 
			// Player2Details
			// 
			this.Player2Details.AutoSize = true;
			this.Player2Details.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Player2Details.Location = new System.Drawing.Point(135, 100);
			this.Player2Details.Name = "Player2Details";
			this.Player2Details.Size = new System.Drawing.Size(60, 50);
			this.Player2Details.TabIndex = 11;
			this.Player2Details.Text = "label3";
			this.Player2Details.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ServerStatusText
			// 
			this.ServerStatusText.AutoSize = true;
			this.GameInfoLayout.SetColumnSpan(this.ServerStatusText, 4);
			this.ServerStatusText.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ServerStatusText.Location = new System.Drawing.Point(3, 250);
			this.ServerStatusText.Name = "ServerStatusText";
			this.ServerStatusText.Size = new System.Drawing.Size(261, 50);
			this.ServerStatusText.TabIndex = 12;
			this.ServerStatusText.Text = "Server Status";
			this.ServerStatusText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ConsoleOutput
			// 
			this.tableLayoutPanel1.SetColumnSpan(this.ConsoleOutput, 2);
			this.ConsoleOutput.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ConsoleOutput.FormattingEnabled = true;
			this.ConsoleOutput.Location = new System.Drawing.Point(3, 414);
			this.ConsoleOutput.Name = "ConsoleOutput";
			this.ConsoleOutput.SelectionMode = System.Windows.Forms.SelectionMode.None;
			this.ConsoleOutput.Size = new System.Drawing.Size(540, 97);
			this.ConsoleOutput.TabIndex = 3;
			// 
			// GameWorker
			// 
			this.GameWorker.WorkerReportsProgress = true;
			this.GameWorker.WorkerSupportsCancellation = true;
			this.GameWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.GameWorker_DoWork);
			this.GameWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.GameWorker_ProgressChanged);
			this.GameWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.GameWorker_RunWorkerCompleted);
			// 
			// GameView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(546, 514);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "GameView";
			this.Text = "GameView";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GameView_FormClosing);
			this.tableLayoutPanel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.BoardImage)).EndInit();
			this.GameInfoLayout.ResumeLayout(false);
			this.GameInfoLayout.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Player1Piece)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Player2Piece)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.PictureBox BoardImage;
		private System.Windows.Forms.TableLayoutPanel GameInfoLayout;
		private System.Windows.Forms.Label InfoTitle;
		private System.Windows.Forms.ListBox ConsoleOutput;
		private System.Windows.Forms.Label Player1Label;
		private System.Windows.Forms.Label Player2Label;
		private System.Windows.Forms.Label TurnLabel;
		private System.Windows.Forms.Label TimerLabel;
		private System.Windows.Forms.Label GameTimerText;
		private System.Windows.Forms.Label Player1Details;
		private System.Windows.Forms.PictureBox Player1Piece;
		private System.Windows.Forms.PictureBox Player2Piece;
		private System.Windows.Forms.Label Player2Details;
		private System.ComponentModel.BackgroundWorker GameWorker;
		private System.Windows.Forms.Label ServerStatusText;
	}
}
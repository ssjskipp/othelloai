﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OthelloAI
{
    public partial class ServerGame : Form
    {
        public ServerGame()
        {
            InitializeComponent();

            foreach (string PlayerType in Globals.PlayerOptionsEnum)
            {
                LocalPlayerType.Items.Add(PlayerType);
            }

			LocalPlayerType.SelectedIndex = 0;
        }

        private void logOutput(string x)
        {
            Console.WriteLine(x);
        }

        private void StartBtn_Click(object sender, EventArgs e)
        {
			//Check formatting.
			int tempPort = 0;
			if (!int.TryParse(PortNumberInput.Text, out tempPort))
			{
				PortNumberInput.Text = "";
				StartBtn.Text = "Start Server Game - Invalid Port Number";
			}
			else
			{
				GameData newGame = new GameData();
				newGame.Player1.Type = LocalPlayerType.SelectedItem.ToString();  //We want Player 1 :D
				newGame.Player1.Name = TeamNameInput.Text;						 //Local Name (to send to server)
				newGame.Player2.Type = Globals.ServerType;						 //Player 2 can be the server
				newGame.ServerIP = IPAddressInput.Text;
				newGame.ServerPort = PortNumberInput.Text;

				MainMenu.PTR.LoadGameData(newGame);
				Hide();
			}
        }

    }
}

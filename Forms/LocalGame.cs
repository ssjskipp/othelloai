﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OthelloAI
{
	public partial class LocalGame : Form
	{

		bool player1White = true;

		public LocalGame()
		{
			InitializeComponent();

			foreach (string PlayerType in Globals.PlayerOptionsEnum)
			{
				Player1Type.Items.Add(PlayerType);
				Player2Type.Items.Add(PlayerType);
			}

			Player1Type.SelectedIndex = 0;
			Player2Type.SelectedIndex = 0;

			toggleColors();
		}

		private void toggleColors()
		{
			player1White = !player1White;

			Player1Color.Checked = !player1White;
			Player2Color.Checked = player1White;
		}

		private void StartBtn_Click(object sender, EventArgs e)
		{
			GameData newGame = new GameData();
			newGame.Player1.Type = Player1Type.SelectedItem.ToString();
			newGame.Player2.Type = Player2Type.SelectedItem.ToString();
			newGame.Player1.Color = player1White ? Player.WHITE : Player.BLACK;
			newGame.Player2.Color = !player1White;

			MainMenu.PTR.LoadGameData(newGame);
			Hide();
		}

		private void Player2Color_Click(object sender, EventArgs e)
		{
			if (!player1White) toggleColors();
		}

		private void Player1Color_Click(object sender, EventArgs e)
		{
			if (player1White) toggleColors();
		}

		private void RandomBtn_Click(object sender, EventArgs e)
		{
			if (Globals.RNG.Next(2) > 0)
			{
				toggleColors();
			}
		}
	}
}

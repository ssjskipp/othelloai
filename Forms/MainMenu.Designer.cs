﻿namespace OthelloAI
{
	partial class MainMenu
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
			this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.TitleLabel = new System.Windows.Forms.Label();
			this.ServerBtn = new System.Windows.Forms.Button();
			this.LocalBtn = new System.Windows.Forms.Button();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.Controls.Add(this.TitleLabel, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.ServerBtn, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.LocalBtn, 1, 2);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 4;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.Size = new System.Drawing.Size(574, 312);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// TitleLabel
			// 
			this.TitleLabel.AutoSize = true;
			this.TitleLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TitleLabel.Location = new System.Drawing.Point(89, 0);
			this.TitleLabel.Name = "TitleLabel";
			this.TitleLabel.Size = new System.Drawing.Size(395, 150);
			this.TitleLabel.TabIndex = 0;
			this.TitleLabel.Text = "ReversiAI for CS 541: AI\r\n\r\nChris Federici, Lorenzo Grompone, and Paul DiCola\r\n";
			this.TitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ServerBtn
			// 
			this.ServerBtn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ServerBtn.Location = new System.Drawing.Point(96, 160);
			this.ServerBtn.Margin = new System.Windows.Forms.Padding(10);
			this.ServerBtn.Name = "ServerBtn";
			this.ServerBtn.Size = new System.Drawing.Size(381, 30);
			this.ServerBtn.TabIndex = 1;
			this.ServerBtn.Text = "Server Game";
			this.ServerBtn.UseVisualStyleBackColor = true;
			this.ServerBtn.Click += new System.EventHandler(this.ServerBtn_Click);
			// 
			// LocalBtn
			// 
			this.LocalBtn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LocalBtn.Location = new System.Drawing.Point(96, 210);
			this.LocalBtn.Margin = new System.Windows.Forms.Padding(10);
			this.LocalBtn.Name = "LocalBtn";
			this.LocalBtn.Size = new System.Drawing.Size(381, 30);
			this.LocalBtn.TabIndex = 2;
			this.LocalBtn.Text = "Local Game";
			this.LocalBtn.UseVisualStyleBackColor = true;
			this.LocalBtn.Click += new System.EventHandler(this.LocalBtn_Click);
			// 
			// MainMenu
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(574, 312);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainMenu";
			this.Text = "ReversiClient - Razzmataz";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.ComponentModel.BackgroundWorker backgroundWorker1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label TitleLabel;
		private System.Windows.Forms.Button ServerBtn;
		private System.Windows.Forms.Button LocalBtn;
	}
}


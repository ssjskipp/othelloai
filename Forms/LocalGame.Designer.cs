﻿namespace OthelloAI
{
	partial class LocalGame
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LocalGame));
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.Player2Settings = new System.Windows.Forms.TableLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.Player2Type = new System.Windows.Forms.ComboBox();
			this.Player2Color = new System.Windows.Forms.RadioButton();
			this.Player1Settings = new System.Windows.Forms.TableLayoutPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.Player1Type = new System.Windows.Forms.ComboBox();
			this.Player1Color = new System.Windows.Forms.RadioButton();
			this.Title = new System.Windows.Forms.Label();
			this.StartBtn = new System.Windows.Forms.Button();
			this.RandomBtn = new System.Windows.Forms.Button();
			this.tableLayoutPanel1.SuspendLayout();
			this.Player2Settings.SuspendLayout();
			this.Player1Settings.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.Controls.Add(this.Player2Settings, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.Player1Settings, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.Title, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.StartBtn, 1, 4);
			this.tableLayoutPanel1.Controls.Add(this.RandomBtn, 1, 3);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 6;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.Size = new System.Drawing.Size(477, 394);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// Player2Settings
			// 
			this.Player2Settings.ColumnCount = 3;
			this.Player2Settings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.Player2Settings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.Player2Settings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.Player2Settings.Controls.Add(this.label1, 0, 0);
			this.Player2Settings.Controls.Add(this.Player2Type, 1, 0);
			this.Player2Settings.Controls.Add(this.Player2Color, 2, 0);
			this.Player2Settings.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Player2Settings.Location = new System.Drawing.Point(75, 131);
			this.Player2Settings.Name = "Player2Settings";
			this.Player2Settings.RowCount = 1;
			this.Player2Settings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Player2Settings.Size = new System.Drawing.Size(325, 69);
			this.Player2Settings.TabIndex = 7;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(59, 69);
			this.label1.TabIndex = 0;
			this.label1.Text = "Player 2 Settings";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Player2Type
			// 
			this.Player2Type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.Player2Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.Player2Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Player2Type.FormattingEnabled = true;
			this.Player2Type.Location = new System.Drawing.Point(68, 22);
			this.Player2Type.Name = "Player2Type";
			this.Player2Type.Size = new System.Drawing.Size(221, 24);
			this.Player2Type.TabIndex = 1;
			// 
			// Player2Color
			// 
			this.Player2Color.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.Player2Color.AutoSize = true;
			this.Player2Color.Location = new System.Drawing.Point(301, 28);
			this.Player2Color.Name = "Player2Color";
			this.Player2Color.Size = new System.Drawing.Size(14, 13);
			this.Player2Color.TabIndex = 2;
			this.Player2Color.TabStop = true;
			this.Player2Color.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.Player2Color.UseVisualStyleBackColor = true;
			this.Player2Color.Click += new System.EventHandler(this.Player2Color_Click);
			// 
			// Player1Settings
			// 
			this.Player1Settings.ColumnCount = 3;
			this.Player1Settings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.Player1Settings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.Player1Settings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.Player1Settings.Controls.Add(this.label2, 0, 0);
			this.Player1Settings.Controls.Add(this.Player1Type, 1, 0);
			this.Player1Settings.Controls.Add(this.Player1Color, 2, 0);
			this.Player1Settings.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Player1Settings.Location = new System.Drawing.Point(75, 55);
			this.Player1Settings.Name = "Player1Settings";
			this.Player1Settings.RowCount = 1;
			this.Player1Settings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Player1Settings.Size = new System.Drawing.Size(325, 69);
			this.Player1Settings.TabIndex = 6;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(3, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(59, 69);
			this.label2.TabIndex = 0;
			this.label2.Text = "Player 1 Settings";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Player1Type
			// 
			this.Player1Type.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.Player1Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.Player1Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Player1Type.FormattingEnabled = true;
			this.Player1Type.Location = new System.Drawing.Point(68, 22);
			this.Player1Type.Name = "Player1Type";
			this.Player1Type.Size = new System.Drawing.Size(221, 24);
			this.Player1Type.TabIndex = 1;
			// 
			// Player1Color
			// 
			this.Player1Color.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.Player1Color.AutoSize = true;
			this.Player1Color.Location = new System.Drawing.Point(301, 28);
			this.Player1Color.Name = "Player1Color";
			this.Player1Color.Size = new System.Drawing.Size(14, 13);
			this.Player1Color.TabIndex = 2;
			this.Player1Color.TabStop = true;
			this.Player1Color.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.Player1Color.UseVisualStyleBackColor = true;
			this.Player1Color.Click += new System.EventHandler(this.Player1Color_Click);
			// 
			// Title
			// 
			this.Title.AutoSize = true;
			this.Title.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Title.Location = new System.Drawing.Point(75, 1);
			this.Title.Name = "Title";
			this.Title.Size = new System.Drawing.Size(325, 50);
			this.Title.TabIndex = 0;
			this.Title.Text = "Local Game Settings";
			this.Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// StartBtn
			// 
			this.StartBtn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StartBtn.Location = new System.Drawing.Point(82, 265);
			this.StartBtn.Margin = new System.Windows.Forms.Padding(10);
			this.StartBtn.Name = "StartBtn";
			this.StartBtn.Size = new System.Drawing.Size(311, 30);
			this.StartBtn.TabIndex = 1;
			this.StartBtn.Text = "Start Local Game";
			this.StartBtn.UseVisualStyleBackColor = true;
			this.StartBtn.Click += new System.EventHandler(this.StartBtn_Click);
			// 
			// RandomBtn
			// 
			this.RandomBtn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RandomBtn.Location = new System.Drawing.Point(82, 214);
			this.RandomBtn.Margin = new System.Windows.Forms.Padding(10);
			this.RandomBtn.Name = "RandomBtn";
			this.RandomBtn.Size = new System.Drawing.Size(311, 30);
			this.RandomBtn.TabIndex = 4;
			this.RandomBtn.Text = "Randomize Color Choices";
			this.RandomBtn.UseVisualStyleBackColor = true;
			this.RandomBtn.Click += new System.EventHandler(this.RandomBtn_Click);
			// 
			// LocalGame
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(477, 394);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "LocalGame";
			this.Text = "LocalGame";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.Player2Settings.ResumeLayout(false);
			this.Player2Settings.PerformLayout();
			this.Player1Settings.ResumeLayout(false);
			this.Player1Settings.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label Title;
		private System.Windows.Forms.Button StartBtn;
		private System.Windows.Forms.Button RandomBtn;
		private System.Windows.Forms.TableLayoutPanel Player2Settings;
		private System.Windows.Forms.TableLayoutPanel Player1Settings;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox Player2Type;
		private System.Windows.Forms.RadioButton Player2Color;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.RadioButton Player1Color;
		private System.Windows.Forms.ComboBox Player1Type;
	}
}
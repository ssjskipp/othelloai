﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OthelloAI
{

	delegate void InfoCallback(string Info);

	public partial class GameView : Form
	{
		public static int Communication = -1;
		public static Point GUISubmitMove = new Point(-1, -1);
		static Point LastSelectedPiece = new Point(-1, -1);

		Bitmap BoardBitmap;

		ReversiGame Game;

		GameData currentGameData;

		/* GameView is the GUI for a game.
		 * The game runs on a seperate thread, and broadcasts events
		 *		to the gui.
		 * 
		 * The game handles network play as well.
		 * 
		 * Windows Form offers a BackgroundWorker class.
		 *		It's nifty...
		 *			..In the toolbox.
		 */ 
		public GameView()
		{
			InitializeComponent();

			int size = Math.Min(BoardImage.Size.Width, BoardImage.Size.Height);

            BoardBitmap = new Bitmap(size + 10, size + 10);
            // (I added the + 10 cause it was clipping without this) 
			BoardImage.Image = BoardBitmap;

			Game = new ReversiGame();
			DrawBoard(null);

			Globals.PostInfo += new InfoPost(InfoPosted);
		}

		public void Reset()
		{
			currentGameData = null;
			Game.Reset();
			DrawBoard(null);
		}

		public void update()
		{
			if (currentGameData != null)
			{
				long minutes = currentGameData.ElapsedTime / 60;
				long seconds = currentGameData.ElapsedTime % 60;

				Player1Label.Text = currentGameData.Player1.ToString();
				Player2Label.Text = currentGameData.Player2.ToString();

				GameTimerText.Text = string.Format("{0:D2}:{1:D2}", minutes, seconds);

				if (Communication == 0)
				{
					Dictionary<int, Piece> moves = currentGameData.GameBoard.PossibleMoves(currentGameData.CurrentPlayer.Color == Player.WHITE);
					int index = OthelloBoard.XYToInt(LastSelectedPiece.X, LastSelectedPiece.Y);
					if (moves.ContainsKey(index))
					{
						GUISubmitMove.X = LastSelectedPiece.X;
						GUISubmitMove.Y = LastSelectedPiece.Y;
						Communication = 1;
					}
					else
					{
						foreach (Piece move in moves.Values)
						{
							Console.Write(move);
						}
						Console.WriteLine("Error. Chosen move is invalid. {0}", currentGameData.CurrentPlayer.Color == Player.WHITE);
						Console.WriteLine(currentGameData.GameBoard);
					}
				}

				if (Game.Server.isConnected())
				{
					ServerStatusText.Text = "Connected. Playing.";
				}
			}
			else
			{
				//TODO: Render error state
				ServerStatusText.Text = "Error. No game data found.";
			}
		}

		public void Configure(GameData data)
		{
			currentGameData = data;
			Communication = -1;
			
			ConsoleOutput.Items.Clear();

			switch (data.Player1.Type)
			{
				case "AILvl01":
					data.Player1.AI = new AILvl01();
					break;
				case "DantAI":
					data.Player1.AI = new DantAI();
					break;
				case "Mover":
					data.Player1.AI = new MoverAI();
					break;
				case "Naive AI":
					data.Player1.AI = new NaiveAI();
					break;
			}

			switch (data.Player2.Type)
			{
				case "AILvl01":
					data.Player2.AI = new AILvl01();
					break;
				case "DantAI":
					data.Player2.AI = new DantAI();
					break;
				case "Mover":
					data.Player2.AI = new MoverAI();
					break;
				case "Naive AI":
					data.Player2.AI = new NaiveAI();
					break;
			}

			//Thread creates its own clone for the game data
			//DOES NOT CLONE AI.
			//AI IS STILL JUST REFERENCES
			//Should be okay, though, since types don't change during gameplay.
			Game.Configure(currentGameData);

			//Start the worker thread
			GameWorker.RunWorkerAsync();

			Globals.PostInfo(string.Format("[{0:HH':'mm':'ss}] GameView configured with: {1}", DateTime.Now, data.ToString()));
			update();
		}

		public void InfoPosted(string info)
		{
			
			if (ConsoleOutput.InvokeRequired)
			{
				InfoCallback d = new InfoCallback(InfoPosted);
				this.Invoke(d, new object[]{ info });
			}
			else
			{
				ConsoleOutput.Items.Add(info);

				int visibleItems = ConsoleOutput.ClientSize.Height / ConsoleOutput.ItemHeight;
				ConsoleOutput.TopIndex = Math.Max(ConsoleOutput.Items.Count - visibleItems, 0);
			}

			Console.WriteLine("[GlobalInfo] {0}", info);
		}
		
		//TODO: Draw the passed BoardState to the bitmap
		public void DrawBoard(OthelloBoard BoardState)
		{
			//Default null to a new/empty board
			if (BoardState == null)
			{
				if (currentGameData == null)
				{
					BoardState = new OthelloBoard();
				}
				else
				{
					BoardState = currentGameData.GameBoard;
				}
			}

			//Draw the bitmap representation of the current board
			Graphics BoardGfx = Graphics.FromImage(BoardBitmap);

			//Clear to red felt
			BoardGfx.Clear(Color.DarkRed);

			//Determine board information
			int size = Math.Min(BoardBitmap.Width, BoardBitmap.Height);
			int numCols = 8;
			int numRows = 8;
			int pieceSize = size / 8;
			int padding = 5;
			Rectangle DrawRect = new Rectangle(0, 0, pieceSize - 2*padding, pieceSize - 2*padding);

			//Draw the tiles down
			foreach (Piece gameTile in BoardState.BlackPieces.Values)
			{
				DrawRect.X = gameTile.x * pieceSize + padding;
				DrawRect.Y = gameTile.y * pieceSize + padding;
				BoardGfx.FillEllipse(Brushes.Black, DrawRect);
			}

			foreach (Piece gameTile in BoardState.WhitePieces.Values)
			{
				DrawRect.X = gameTile.x * pieceSize + padding;
				DrawRect.Y = gameTile.y * pieceSize + padding;
				BoardGfx.FillEllipse(Brushes.White, DrawRect);
			}


			Pen BoardLines = new Pen(Color.Black, padding);
			//Draw the grid over them, 8 rows and 8 cols, one extra because borders
			for (int index = 0; index <= numRows; index++)
			{
				//Draw from top to bottom
				BoardGfx.DrawLine(BoardLines, index * pieceSize, 0, index * pieceSize, size);

				//And from left to right
				BoardGfx.DrawLine(BoardLines, 0, index * pieceSize, size, index * pieceSize);
			}

			//Now draw the selected cell
			if (LastSelectedPiece.X >= 0 && LastSelectedPiece.X <= 7 && LastSelectedPiece.Y >= 0 && LastSelectedPiece.Y <= 7)
			{
				BoardLines.Color = Color.Green;
				DrawRect.X = LastSelectedPiece.X * pieceSize;
				DrawRect.Y = LastSelectedPiece.Y * pieceSize;
				DrawRect.Width = pieceSize;
				DrawRect.Height = pieceSize;
				BoardGfx.DrawRectangle(BoardLines, DrawRect);
			}

			BoardImage.Refresh();
		}

		//TODO: Handle the mouse input in some fashion. Derive the (x, y) position of the clicked game tile
		private void BoardImage_Click(object sender, EventArgs e)
		{
            PictureBox Board = (PictureBox)sender;
            MouseEventArgs me = (MouseEventArgs)e;

			//Offsets
			int ySpace = Board.Height - BoardBitmap.Height;
			int xSpace = Board.Width - BoardBitmap.Width;
			int pieceSize = Math.Min(BoardBitmap.Height, BoardBitmap.Width) / 8;

			int x = (me.Location.X - xSpace / 2) / pieceSize;
			int y = (me.Location.Y - ySpace / 2) / pieceSize;

			if (x >= 0 && x <= 7 && y >= 0 && y <= 7)
			{
				LastSelectedPiece.X = x;
				LastSelectedPiece.Y = y;
				update();
			}
		}

		/* These represent the game worker thread.
		 * Use ReportProgress to send data back and forth.
		 * DoWork represents 1 iteration of "work".
		 * That would be 1 call to the server client, or what have you.
		 * In this case, 1 instance of "ReversiGame.update();"
		 */ 
		private void GameWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			bool exitStatus;
			BackgroundWorker asyncWorker = sender as BackgroundWorker;

			//This is the thread function. In here, the Game is running
			exitStatus = Game.Run(asyncWorker);

			e.Result = Game.GameState;

			//Exit gracefully
			if (exitStatus)
			{
				//Should probably update gui or something?
				InfoPosted("Game ended.");
			}
			//Have to quit.
			else
			{
				e.Cancel = true;
				InfoPosted("Quitting...");
			}
		}

		/* When Game.Run() spits out a worker spits out "ReportProgress( % complete, GameData )"
		 * This gets that message.
		 * So change GUI to the reports of the GameState.
		 * 
		 * 10 = Request GUI point
		 */ 
		private void GameWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			GameData GameState = e.UserState as GameData;

			if (currentGameData.ElapsedTime <= GameState.ElapsedTime)
			{
				currentGameData = GameState;
			}

			Console.WriteLine(currentGameData.GameBoard);

			if (currentGameData.Player1.Color == Player.WHITE)
			{
				Player1Details.Text = "Score: " + currentGameData.GameBoard.WhitePieces.Count.ToString();
				Player2Details.Text = "Score: " + currentGameData.GameBoard.BlackPieces.Count.ToString();
			}
			else
			{
				Player1Details.Text = "Score: " + currentGameData.GameBoard.BlackPieces.Count.ToString();
				Player2Details.Text = "Score: " + currentGameData.GameBoard.WhitePieces.Count.ToString();
			}

			DrawBoard(currentGameData.GameBoard);

			//React to different messages
			if (e.ProgressPercentage == 10)
			{
				//Enter request for GUI point
				Communication = 0;
				TurnLabel.Text = string.Format("[{0}]{1}'s turn. Click the board to move.", currentGameData.CurrentPlayer.Color == Player.WHITE ? "White" : "Black", currentGameData.CurrentPlayer.Name);
			}
			else if (e.ProgressPercentage == 11)
			{
				//Gui event has been consumed
				Communication = -1;
			}

			update();
		}

		private void GameView_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (GameWorker.IsBusy)
			{
				GameWorker.CancelAsync();
				InfoPosted("Closing the game...");
				e.Cancel = true;
			}
		}

		private void BoardImage_SizeChanged(object sender, EventArgs e)
		{
			int newBitmapSize = Math.Min(BoardImage.Size.Width, BoardImage.Size.Height);
			
			BoardBitmap.Dispose();
			BoardBitmap = new Bitmap(newBitmapSize, newBitmapSize);
			BoardImage.Image = BoardBitmap;

			DrawBoard(currentGameData.GameBoard);
		}

		private void GameWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			currentGameData = e.Result as GameData;
			
			InfoPosted("Game over!");
			
			if (currentGameData.winValue == 0)
			{
				if (currentGameData.isServerGame)
				{
					InfoPosted("You lose!");
				}
				else
				{
					InfoPosted("Black wins!");
				}
			}
			else if (currentGameData.winValue == 1)
			{
				if (currentGameData.isServerGame)
				{
					InfoPosted("You win!");
				}
				else
				{
					InfoPosted("White wins!");
				}
			}
			else
			{
				InfoPosted("It's a tie!");
			}

			if (e.Cancelled == true)
			{
				Close();
			}

			DrawBoard(currentGameData.GameBoard);

			update();
		}
	}
}

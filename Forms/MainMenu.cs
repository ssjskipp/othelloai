﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OthelloAI
{
	public partial class MainMenu : Form
	{
		public static MainMenu PTR;

		public LocalGame LocalGameForm;
        public ServerGame ServerGameForm;
		public GameView GameViewForm;

		public MainMenu()
		{
			PTR = this;

			InitializeComponent();

			LocalGameForm = new LocalGame();
			LocalGameForm.FormClosing += localGameForm_FormClosing;

            ServerGameForm = new ServerGame();
			ServerGameForm.FormClosing += ServerGameForm_FormClosing;

			GameViewForm = new GameView();
			GameViewForm.FormClosing += GameViewForm_FormClosing;

			//Try to make some moves
			/*tmp.MakeMove(2, 3, false);
			Console.WriteLine(tmp.ToString());
			Console.Write("White: ");
			foreach (Piece move in tmp.PossibleMoves(true).Values)
			{
				Console.Write(move.ToString());
			}
			Console.Write('\n');
			Console.Write("Black: ");
			foreach (Piece move in tmp.PossibleMoves(false).Values)
			{
				Console.Write(move.ToString());
			}
			Console.Write('\n');

			//Final board state
			Console.WriteLine(tmp.ToString());*/
		}

		/* Loads a new GameWindow with the supplied GameData instance.
		 * Does not support 2 games locally.
		 * Should consider that, but for now replaces old with new.
		 */
		public void LoadGameData(GameData Data)
		{
			if (GameViewForm.Visible)
			{
				GameViewForm.Reset();
			}
			else
			{
				GameViewForm.Show();
			}
			GameViewForm.Configure(Data);
		}

		void GameViewForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (!e.Cancel)
			{
				Show();
				GameViewForm.Reset();
				GameViewForm.Hide();
				e.Cancel = true;
			}
		}

		void localGameForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			Show();
			LocalGameForm.Hide();
			e.Cancel = true;
		}

		void ServerGameForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			Show();
			ServerGameForm.Hide();
			e.Cancel = true;
		}

		private void ServerBtn_Click(object sender, EventArgs e)
		{
            ServerGameForm.Show();
            Hide();
		}
		
		private void LocalBtn_Click(object sender, EventArgs e)
		{
			LocalGameForm.Show();
			Hide();
		}
	}
}

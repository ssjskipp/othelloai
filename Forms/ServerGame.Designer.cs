﻿namespace OthelloAI
{
    partial class ServerGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServerGame));
			this.Title = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.LocalPlayerSettings = new System.Windows.Forms.TableLayoutPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.LocalPlayerType = new System.Windows.Forms.ComboBox();
			this.StartBtn = new System.Windows.Forms.Button();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.TeamNameInput = new System.Windows.Forms.TextBox();
			this.PortNumberInput = new System.Windows.Forms.TextBox();
			this.IPAddressInput = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel1.SuspendLayout();
			this.LocalPlayerSettings.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// Title
			// 
			this.Title.AutoSize = true;
			this.Title.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Title.Location = new System.Drawing.Point(18, 1);
			this.Title.Name = "Title";
			this.Title.Size = new System.Drawing.Size(385, 50);
			this.Title.TabIndex = 0;
			this.Title.Text = "Server Game Settings";
			this.Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel1.ColumnCount = 3;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.167421F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 88.68778F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
			this.tableLayoutPanel1.Controls.Add(this.LocalPlayerSettings, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.Title, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.StartBtn, 1, 4);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 2);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 6;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 76F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.Size = new System.Drawing.Size(443, 376);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// LocalPlayerSettings
			// 
			this.LocalPlayerSettings.ColumnCount = 2;
			this.LocalPlayerSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.27177F));
			this.LocalPlayerSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.72823F));
			this.LocalPlayerSettings.Controls.Add(this.label2, 0, 0);
			this.LocalPlayerSettings.Controls.Add(this.LocalPlayerType, 1, 0);
			this.LocalPlayerSettings.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LocalPlayerSettings.Location = new System.Drawing.Point(18, 55);
			this.LocalPlayerSettings.Name = "LocalPlayerSettings";
			this.LocalPlayerSettings.RowCount = 1;
			this.LocalPlayerSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.LocalPlayerSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
			this.LocalPlayerSettings.Size = new System.Drawing.Size(385, 64);
			this.LocalPlayerSettings.TabIndex = 6;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(3, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(160, 64);
			this.label2.TabIndex = 0;
			this.label2.Text = "Local Player Settings";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// LocalPlayerType
			// 
			this.LocalPlayerType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.LocalPlayerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.LocalPlayerType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LocalPlayerType.FormattingEnabled = true;
			this.LocalPlayerType.Location = new System.Drawing.Point(169, 19);
			this.LocalPlayerType.Name = "LocalPlayerType";
			this.LocalPlayerType.Size = new System.Drawing.Size(213, 24);
			this.LocalPlayerType.TabIndex = 1;
			// 
			// StartBtn
			// 
			this.StartBtn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StartBtn.Location = new System.Drawing.Point(25, 265);
			this.StartBtn.Margin = new System.Windows.Forms.Padding(10);
			this.StartBtn.Name = "StartBtn";
			this.StartBtn.Size = new System.Drawing.Size(371, 30);
			this.StartBtn.TabIndex = 1;
			this.StartBtn.Text = "Start Server Game";
			this.StartBtn.UseVisualStyleBackColor = true;
			this.StartBtn.Click += new System.EventHandler(this.StartBtn_Click);
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 2;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.label3, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this.label4, 0, 2);
			this.tableLayoutPanel2.Controls.Add(this.TeamNameInput, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.PortNumberInput, 1, 1);
			this.tableLayoutPanel2.Controls.Add(this.IPAddressInput, 1, 2);
			this.tableLayoutPanel2.Location = new System.Drawing.Point(18, 126);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 3;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(382, 70);
			this.tableLayoutPanel2.TabIndex = 7;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(62, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "TeamName";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(3, 25);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(63, 13);
			this.label3.TabIndex = 1;
			this.label3.Text = "PortNumber";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(3, 50);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(55, 13);
			this.label4.TabIndex = 2;
			this.label4.Text = "IPAddress";
			// 
			// TeamNameInput
			// 
			this.TeamNameInput.Location = new System.Drawing.Point(194, 3);
			this.TeamNameInput.Name = "TeamNameInput";
			this.TeamNameInput.Size = new System.Drawing.Size(100, 20);
			this.TeamNameInput.TabIndex = 3;
			this.TeamNameInput.Text = "Razzmatazz";
			// 
			// PortNumberInput
			// 
			this.PortNumberInput.Location = new System.Drawing.Point(194, 28);
			this.PortNumberInput.Name = "PortNumberInput";
			this.PortNumberInput.Size = new System.Drawing.Size(100, 20);
			this.PortNumberInput.TabIndex = 4;
			this.PortNumberInput.Text = "4000";
			// 
			// IPAddressInput
			// 
			this.IPAddressInput.Location = new System.Drawing.Point(194, 53);
			this.IPAddressInput.Name = "IPAddressInput";
			this.IPAddressInput.Size = new System.Drawing.Size(100, 20);
			this.IPAddressInput.TabIndex = 5;
			this.IPAddressInput.Text = "127.0.0.1";
			// 
			// ServerGame
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(443, 376);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ServerGame";
			this.Text = "ServerGame";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.LocalPlayerSettings.ResumeLayout(false);
			this.LocalPlayerSettings.PerformLayout();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel LocalPlayerSettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox LocalPlayerType;
        private System.Windows.Forms.Button StartBtn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TeamNameInput;
        private System.Windows.Forms.TextBox PortNumberInput;
        private System.Windows.Forms.TextBox IPAddressInput;
    }
}
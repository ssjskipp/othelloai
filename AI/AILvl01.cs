﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OthelloAI
{
    public class AILvl01 : BaseAI 
    {
        int searches;
        public AILvl01()
        {
            searches = 0;
        }

        public override void MakeMove(out int x, out int y)
        {
            x = y = -1;

            OthelloBoard PassState;
            Dictionary<int, Piece> Reaction;
            OthelloBoard ReturnState;

            Piece currMove;
            currMove = null;
            Piece bestMove;
            bestMove = new Piece(-1, -1, 0);
            float currValue;
            currValue = float.MaxValue;
            float bestValue = float.MinValue;
            bestValue = 0.0f;
            float tempValue;
            tempValue = 0.0f;

            Dictionary<int, Piece> moveSet = KnownBoard.PossibleMoves(isWhite);
            if(isWhite &&  (KnownBoard.BlackPieces.Count + KnownBoard.WhitePieces.Count) == 5)
            {
                // FIRST WHITE MOVE STRATEGY (never open parallel, currently opens diagonal)
                foreach(Piece move in KnownBoard.WhitePieces.Values)
                {
                    searches++;
                    // Written in a way that we can expand if we know more opening tricks
                    // There is one white piece, and we'll move depending on what it is
                    if(move.x == 4)
                    {
                        bestMove.x = 2;
                        bestMove.y = 2;
                    }
                    else
                    {
                        bestMove.x = 5;
                        bestMove.y = 5;
                    }
                }
                x = bestMove.x;
                y = bestMove.y;
            }

            else if(KnownBoard.BlackPieces.Count + KnownBoard.WhitePieces.Count > 55)
            {
                // END GAME STRATEGY
                bestMove = endGameSearch(KnownBoard, isWhite);
                x = bestMove.x;
                y = bestMove.y;
                Console.WriteLine("EndGame: " + bestMove.ToString() + isWhite);
            }
            else
            {
                // MID GAME STRATEGY
                foreach (Piece move in moveSet.Values)
                {
                    searches++;
                    Console.WriteLine("What if I go " + move.ToString());
                    currMove = move;
                    currValue = float.MaxValue;
                    PassState = KnownBoard.Clone();
                    PassState.MakeMove(move.x, move.y, isWhite);
                    Reaction = PassState.PossibleMoves(!isWhite);
                    foreach (Piece nextMove in Reaction.Values)
                    {
                        searches++;
                        ReturnState = PassState.Clone();
                        ReturnState.MakeMove(nextMove.x, nextMove.y, !isWhite);
                        tempValue = moveValue(ReturnState, isWhite);
                        // Console.WriteLine("Move value is : " + tempValue);
                        if (ReturnState.PossibleMoves(isWhite).ContainsKey(0) || ReturnState.PossibleMoves(isWhite).ContainsKey(7) || ReturnState.PossibleMoves(isWhite).ContainsKey(63) || ReturnState.PossibleMoves(isWhite).ContainsKey(56))
                        {
                            tempValue += 2;
                        }
                        else if (ReturnState.PossibleMoves(isWhite).ContainsKey(2) || ReturnState.PossibleMoves(isWhite).ContainsKey(5) || ReturnState.PossibleMoves(isWhite).ContainsKey(16) || ReturnState.PossibleMoves(isWhite).ContainsKey(23) ||
                            ReturnState.PossibleMoves(isWhite).ContainsKey(61) || ReturnState.PossibleMoves(isWhite).ContainsKey(58) || ReturnState.PossibleMoves(isWhite).ContainsKey(47) || ReturnState.PossibleMoves(isWhite).ContainsKey(40))
                        {
                            tempValue += 1;
                        }
                        else if (ReturnState.PossibleMoves(isWhite).ContainsKey(1) || ReturnState.PossibleMoves(isWhite).ContainsKey(6) || ReturnState.PossibleMoves(isWhite).ContainsKey(8) || ReturnState.PossibleMoves(isWhite).ContainsKey(15) ||
                            ReturnState.PossibleMoves(isWhite).ContainsKey(62) || ReturnState.PossibleMoves(isWhite).ContainsKey(55) || ReturnState.PossibleMoves(isWhite).ContainsKey(57) || ReturnState.PossibleMoves(isWhite).ContainsKey(49) ||
                            ReturnState.PossibleMoves(isWhite).ContainsKey(9) || ReturnState.PossibleMoves(isWhite).ContainsKey(14) || ReturnState.PossibleMoves(isWhite).ContainsKey(54) || ReturnState.PossibleMoves(isWhite).ContainsKey(50))
                        {
                            tempValue -= 3;
                        }
                        if (PassState.PossibleMoves(!isWhite).ContainsKey(0) || PassState.PossibleMoves(!isWhite).ContainsKey(7) || PassState.PossibleMoves(!isWhite).ContainsKey(63) || PassState.PossibleMoves(!isWhite).ContainsKey(56))
                        {
                            tempValue -= 50;
                        }
                        else if (PassState.PossibleMoves(!isWhite).ContainsKey(2) || PassState.PossibleMoves(!isWhite).ContainsKey(5) || PassState.PossibleMoves(!isWhite).ContainsKey(16) || PassState.PossibleMoves(!isWhite).ContainsKey(23) ||
                        PassState.PossibleMoves(!isWhite).ContainsKey(61) || PassState.PossibleMoves(!isWhite).ContainsKey(58) || PassState.PossibleMoves(!isWhite).ContainsKey(47) || PassState.PossibleMoves(!isWhite).ContainsKey(40))
                        {
                            tempValue -= 10;
                        }
                        else if (PassState.PossibleMoves(!isWhite).ContainsKey(1) || PassState.PossibleMoves(!isWhite).ContainsKey(6) || PassState.PossibleMoves(!isWhite).ContainsKey(8) || PassState.PossibleMoves(!isWhite).ContainsKey(15) ||
                            PassState.PossibleMoves(!isWhite).ContainsKey(62) || PassState.PossibleMoves(!isWhite).ContainsKey(55) || PassState.PossibleMoves(!isWhite).ContainsKey(57) || PassState.PossibleMoves(!isWhite).ContainsKey(49) ||
                            PassState.PossibleMoves(!isWhite).ContainsKey(9) || PassState.PossibleMoves(!isWhite).ContainsKey(14) || PassState.PossibleMoves(!isWhite).ContainsKey(54) || PassState.PossibleMoves(!isWhite).ContainsKey(50))
                        {
                            tempValue += 2;
                        }
                    }
                    if (((currMove.x == 0) && (currMove.y == 1 || currMove.y == 6)) || ((currMove.x == 1) && (currMove.y == 0 || currMove.y == 7)) || ((currMove.x == 6 && (currMove.y == 0 || currMove.y == 7))) || ((currMove.x == 7) && (currMove.y == 1 || currMove.y == 6)))
                    {
                        tempValue -= 20;
                    }
                    if (((currMove.x == 0 || currMove.x == 7) && (currMove.y == 2 || currMove.y == 5)) || (currMove.x == 2 || currMove.x == 5) && (currMove.y == 0 || currMove.y == 7))
                    {
                        tempValue += 4;
                    }
                    if ((currMove.x == 0 && (currMove.y == 0 || currMove.y == 7)) || (currMove.x == 7) && (currMove.y == 0 || currMove.y == 7))
                    {
                        tempValue += 100;
                    }

                    currValue = Math.Min(tempValue, currValue);
                    // Console.WriteLine("TempValue became : " + tempValue);

                    if (bestMove.x == -1 || currValue > bestValue)
                    {
                        bestValue = currValue;
                        bestMove.x = currMove.x;
                        bestMove.y = currMove.y;
                        bestMove.cell = currMove.cell;
                    }
                    Console.WriteLine("So that means I should go " + bestMove.ToString());
                }
                x = bestMove.x;
                y = bestMove.y;
            }
			
            if (x < 0 || y < 0)
            {
                //Error.
                Console.WriteLine("Error. {0}", moveSet.Count);
            }
            else
            {
                Console.WriteLine("{0},{1},{2}", x, y, isWhite);
                KnownBoard.MakeMove(x, y, isWhite);
                Console.WriteLine(KnownBoard);
            }
            Console.WriteLine("I've done " + searches + " searches so far");
		}

        public float moveValue(OthelloBoard board, bool isWhite)
        {
            return board.PossibleMoves(isWhite).Count;
        }

        public float endGameMoveValue(OthelloBoard board, bool isWhite)
        {
            float value = board.WhitePieces.Count - board.BlackPieces.Count;
            if (isWhite)
            {
                return value;
            }
            else
            {
                return -value;
            }
        }

        public Piece endGameSearch(OthelloBoard board, bool isWhite)
        {
            OthelloBoard passState;
            Piece bestMove;
            float currValue;
            float bestValue;
            bestValue = float.MaxValue;
            bestMove = new Piece(-1, -1, 0);
            if(board.PossibleMoves(isWhite).Count == 0)
            {
                searches++;
                return bestMove;
            }
            foreach(Piece move in board.PossibleMoves(isWhite).Values)
            {
                searches++;
                passState = board.Clone();
                passState.MakeMove(move.x, move.y, isWhite);
                currValue = endGameSearchValue(passState, !isWhite);
                if(currValue < bestValue)
                {
                    bestMove = move;
                    bestValue = currValue;
                }
            }
            return bestMove;
        }

        public float endGameSearchValue(OthelloBoard board, bool isWhite)
        {
            Piece bestMove;
            bestMove = endGameSearch(board, isWhite);
            if(bestMove.x == -1)
            {
                return endGameMoveValue(board, isWhite);
            }
            else
            {
                OthelloBoard passState;
                passState = board.Clone();
                passState.MakeMove(bestMove.x, bestMove.y, isWhite);
                return endGameMoveValue(passState, isWhite);
            }
        }
    }
}
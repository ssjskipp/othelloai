﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OthelloAI
{
    public class NaiveAI : BaseAI
    {
		public NaiveAI()
		{

		}

		public override void MakeMove(out int x, out int y)
		{
			x = y = -1;

			Dictionary<int, Piece> moveSet = KnownBoard.PossibleMoves(isWhite);
			if (moveSet.Values.Count > 0)
			{
				int moveIndex = Globals.RNG.Next(1, moveSet.Values.Count);
				foreach (Piece move in moveSet.Values)
				{
					x = move.x;
					y = move.y;
					moveIndex--;

					if (moveIndex < 0)
					{
						break;
					}
				}

				Console.WriteLine("{0},{1},{2}", x, y, isWhite);
				KnownBoard.MakeMove(x, y, isWhite);
				Console.WriteLine(KnownBoard);
			}
			else
			{
				Console.WriteLine("[NaiveAI] Error. Moveset Size= {0}", moveSet.Count);
			}
		}
    }
}

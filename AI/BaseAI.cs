﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OthelloAI
{
	public class BaseAI
	{
		public bool isWhite = false;

		protected OthelloBoard KnownBoard = new OthelloBoard();
		protected Piece LastMove = null;

		public BaseAI()
		{
		}

		public void UpdateBoard(OthelloBoard Board)
        {
			KnownBoard = Board.Clone();
            /*
			Dictionary<int, Piece> OpposingPieces = isWhite ? Board.BlackPieces : Board.WhitePieces;

			//Find the new move
			LastMove = null;
			foreach (Tuple<int, int> pair in KnownBoard.Perimeter)
			{
				if (OpposingPieces.ContainsKey(OthelloBoard.XYToInt(pair.Item1, pair.Item2)))
				{
					LastMove = OpposingPieces[OthelloBoard.XYToInt(pair.Item1, pair.Item2)];
					break;
				}
			}

			//Update the board with the new move
			if (LastMove != null)
			{
				KnownBoard.MakeMove(LastMove.x, LastMove.y, !isWhite);
				Globals.PostInfo("[BaseAI:UpdateBoard] Last move: " + LastMove);
			}
			else
			{
				//Error?
				Globals.PostInfo("[BaseAI:UpdateBoard] Last move not found.");
			}
             */
		}

		public virtual void MakeMove(out int x, out int y)
		{
			x = 0;
			y = 0;
			Console.WriteLine("What?!");
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OthelloAI
{
	public class DantAI : BaseAI
	{

		public DantAI()
		{
		}

		public override void MakeMove(out int x, out int y)
		{
			x = y = -1;
			OthelloBoard CopyBoard;
			OthelloBoard EnemyBoard;
			Dictionary<int, Piece> enemyMoveSet;
			Dictionary<int, Piece> moveSet = KnownBoard.PossibleMoves(isWhite);
			if (moveSet.Values.Count > 0)
			{
				Tuple<Piece, int> worstEnemyMoveset = null; // (My Move, Enemy Moveset Value)
				//For every move I can make
				foreach (Piece move in moveSet.Values)
				{
					CopyBoard = KnownBoard.Clone();
					CopyBoard.MakeMove(move.x, move.y, isWhite);

					//I want to figure out the move I make which yeilds
					//the minimum enemyMoveSetValue.
					int enemyMoveSetValue = 0;

					//This generates a set of possible enemy moves.
					//We want to value how good each of their moves are.
					enemyMoveSet = CopyBoard.PossibleMoves(!isWhite);
					foreach (Piece enemyMove in enemyMoveSet.Values)
					{
						List<Piece> Results = CopyBoard.MoveEffects(enemyMove.x, enemyMove.y, !isWhite);
						//If they make this move, Results is what changes.
						//Let's evaluate the board now
						//And we can check Results for critical tiles
						//And weight them differently
						Dictionary<int, Piece> MyPieces = isWhite ? CopyBoard.WhitePieces : CopyBoard.BlackPieces;
						Dictionary<int, Piece> EnemyPieces = isWhite ? CopyBoard.BlackPieces : CopyBoard.WhitePieces;
						int enemyResult = EnemyPieces.Count + Results.Count;
						int myResult = MyPieces.Count - (Results.Count - 1); // Remove their added piece

						//So, the enemy had a delta of Results.Count
						//And I had a delta of -Results.Count + 1
						//So if I make this move, one of their moves gives this outcome
						enemyMoveSetValue += Results.Count;
						//Current value of a moveset = sum of possible turned tiles
						//Can try a bunch of variables
						//Can do machine learning to find the best ones over entire games against self or random or random mix
					}

					if (worstEnemyMoveset == null || (worstEnemyMoveset.Item2 > enemyMoveSetValue))
					{
						worstEnemyMoveset = Tuple.Create<Piece, int>(move, enemyMoveSetValue);
					}
				}

				//Play the move which yeilds the worst enemy moveset
				x = worstEnemyMoveset.Item1.x;
				y = worstEnemyMoveset.Item1.y;

				Console.WriteLine("{0},{1},{2}", x, y, isWhite);
				KnownBoard.MakeMove(x, y, isWhite);
				Console.WriteLine(KnownBoard);
			}
			else
			{
				Console.WriteLine("[NaiveAI] Error. Moveset Size= {0}", moveSet.Count);
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OthelloAI
{
    public class MoverAI : BaseAI
    {
        public MoverAI()
        {

        }

        public override void MakeMove(out int x, out int y)
        {
            x = y = -1;

            OthelloBoard PassState;
            Dictionary<int, Piece> Reaction;
            OthelloBoard ReturnState;

            Piece currMove;
            currMove = null;
            Piece bestMove;
            bestMove = new Piece(-1, -1, 0);
            float currValue;
            currValue = float.MaxValue;
            float bestValue = float.MinValue;
            bestValue = 0.0f;

            Dictionary<int, Piece> moveSet = KnownBoard.PossibleMoves(isWhite);
                foreach (Piece move in moveSet.Values)
                {
                    Console.WriteLine("What if I go " + move.ToString());
                    currMove = move;
                    currValue = float.MaxValue;
                    PassState = KnownBoard.Clone();
                    PassState.MakeMove(move.x, move.y, isWhite);
                    Reaction = PassState.PossibleMoves(!isWhite);
                    foreach (Piece nextMove in Reaction.Values)
                    {
                        ReturnState = PassState.Clone();
                        ReturnState.MakeMove(nextMove.x, nextMove.y, !isWhite);
                        currValue = Math.Min(moveValue(ReturnState, isWhite), currValue);
                        if (ReturnState.PossibleMoves(isWhite).ContainsKey(0) || ReturnState.PossibleMoves(isWhite).ContainsKey(7) || ReturnState.PossibleMoves(isWhite).ContainsKey(63) || ReturnState.PossibleMoves(isWhite).ContainsKey(56))
                        {
                            currValue += 2;
                        }
                        if (ReturnState.PossibleMoves(isWhite).ContainsKey(1) || ReturnState.PossibleMoves(isWhite).ContainsKey(6) || ReturnState.PossibleMoves(isWhite).ContainsKey(8) || ReturnState.PossibleMoves(isWhite).ContainsKey(15) ||
                            ReturnState.PossibleMoves(isWhite).ContainsKey(62) || ReturnState.PossibleMoves(isWhite).ContainsKey(55) || ReturnState.PossibleMoves(isWhite).ContainsKey(57) || ReturnState.PossibleMoves(isWhite).ContainsKey(49) ||
                            ReturnState.PossibleMoves(isWhite).ContainsKey(9) || ReturnState.PossibleMoves(isWhite).ContainsKey(14) || ReturnState.PossibleMoves(isWhite).ContainsKey(54) || ReturnState.PossibleMoves(isWhite).ContainsKey(50))
                        {
                            currValue -= 2;
                        }
                        if (PassState.PossibleMoves(!isWhite).ContainsKey(0) || PassState.PossibleMoves(!isWhite).ContainsKey(7) || PassState.PossibleMoves(!isWhite).ContainsKey(63) || PassState.PossibleMoves(!isWhite).ContainsKey(56))
                        {
                            currValue -= 2;
                        }
                        if (PassState.PossibleMoves(!isWhite).ContainsKey(1) || PassState.PossibleMoves(!isWhite).ContainsKey(6) || PassState.PossibleMoves(!isWhite).ContainsKey(8) || PassState.PossibleMoves(!isWhite).ContainsKey(15) ||
                            PassState.PossibleMoves(!isWhite).ContainsKey(62) || PassState.PossibleMoves(!isWhite).ContainsKey(55) || PassState.PossibleMoves(!isWhite).ContainsKey(57) || PassState.PossibleMoves(!isWhite).ContainsKey(49) ||
                            PassState.PossibleMoves(!isWhite).ContainsKey(9) || PassState.PossibleMoves(!isWhite).ContainsKey(14) || PassState.PossibleMoves(!isWhite).ContainsKey(54) || PassState.PossibleMoves(!isWhite).ContainsKey(50))
                        {
                            currValue += 2;
                        }
                    }
                    if (((currMove.x == 0) && (currMove.y == 1 || currMove.y == 6)) || ((currMove.x == 1) && (currMove.y == 0 || currMove.y == 7)) || ((currMove.x == 6 && (currMove.y == 0 || currMove.y == 7))) || ((currMove.x == 7) && (currMove.y == 1 || currMove.y == 6)))
                    {
                        currValue -= 10;
                    }
                    if ((currMove.x == 0 && (currMove.y == 0 || currMove.y == 7)) || (currMove.x == 7) && (currMove.y == 0 || currMove.y == 7))
                    {
                        currValue += 10;
                    }

                    if (bestMove.x == -1 || currValue > bestValue)
                    {
                        bestValue = currValue;
                        bestMove.x = currMove.x;
                        bestMove.y = currMove.y;
                        bestMove.cell = currMove.cell;
                    }
                    Console.WriteLine("So that means I should go " + bestMove.ToString());
                }
                x = bestMove.x;
                y = bestMove.y;

            if (x < 0 || y < 0)
            {
                //Error.
                Console.WriteLine("Error. {0}", moveSet.Count);
            }
            else
            {
                Console.WriteLine("{0},{1},{2}", x, y, isWhite);
                KnownBoard.MakeMove(x, y, isWhite);
                Console.WriteLine(KnownBoard);
            }
        }

        public float moveValue(OthelloBoard board, bool isWhite)
        {
            return board.PossibleMoves(isWhite).Count;
        }

        public float endGameMoveValue(OthelloBoard board, bool isWhite)
        {
            float value = board.WhitePieces.Count - board.BlackPieces.Count;
            if (isWhite)
            {
                return value;
            }
            else
            {
                return -value;
            }
        }

        public Piece endGameSearch(OthelloBoard board, bool isWhite)
        {
            OthelloBoard passState;
            Piece bestMove;
            float currValue;
            float bestValue;
            bestValue = float.MaxValue;
            bestMove = new Piece(-1, -1, 0);
            if (board.PossibleMoves(isWhite).Count == 0)
            {
                return bestMove;
            }
            foreach (Piece move in board.PossibleMoves(isWhite).Values)
            {
                passState = board.Clone();
                passState.MakeMove(move.x, move.y, isWhite);
                currValue = endGameSearchValue(passState, !isWhite);
                if (currValue < bestValue)
                {
                    bestMove = move;
                    bestValue = currValue;
                }
            }
            return bestMove;
        }

        public float endGameSearchValue(OthelloBoard board, bool isWhite)
        {
            Piece bestMove;
            bestMove = endGameSearch(board, isWhite);
            if (bestMove.x == -1)
            {
                return endGameMoveValue(board, isWhite);
            }
            else
            {
                OthelloBoard passState;
                passState = board.Clone();
                passState.MakeMove(bestMove.x, bestMove.y, isWhite);
                return endGameMoveValue(passState, isWhite);
            }
        }
    }
}
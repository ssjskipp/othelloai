﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OthelloAI
{
	public class Player
	{
		public const bool WHITE = true;
		public const bool BLACK = false;

		public string Type;
		public string Name;
		public BaseAI AI = null;
		public bool Color;

		public bool isAI
		{
			get
			{
				return AI != null;
			}
		}

		public Player()
		{
		}

		public void Clone(Player Source)
		{
			Type = Source.Type;
			Name = Source.Name;
			AI = Source.AI;
			Color = Source.Color;
		}

		public override string ToString()
		{
			return string.Format("[{0}:{1},{2}]", Name, Type, Color == BLACK ? "Black" : "White");
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OthelloAI
{
	/* Entire game engine
	 * 
	 * Keeps track of its own internal GameState
	 * 
	 * Initial configure resets gamestate
	 * 
	 * DoWorker progress sends up a clone of GameState
	 */

	public class ReversiGame
	{
		const bool PLAYER1_TURN = false;
		const bool PLAYER2_TURN = true;
		const int SERVER_BLACK = 1;

		public GameData GameState = new GameData(); //NEVER WRITE ONLY READ AND HARDLY IF THAT
		bool currentTurn = PLAYER1_TURN;
        bool colorSet = false;

		//Use ServerClient members to
		public ServerClient Server = new ServerClient();
		public bool isPlaying = true;

		//Server-side Stuff
		public int[,] BoardInformation;
		Piece LastMove = null;
		bool resendMove;
		bool waitingMove;
		bool waitOnGui = false;
		
		//What the nuts
		bool hadNoMoves = false;

		Stopwatch timer;

		public ReversiGame()
		{
			Server.OnMessage += new ServerMessage(ServerInformation);

			BoardInformation = new int[8, 8];

			timer = new Stopwatch();
		}

		public void Reset()
		{
			currentTurn = PLAYER1_TURN;
			isPlaying = false;
			resendMove = false;
			waitingMove = false;
			LastMove = null;
			GameState.Reset();
			timer.Reset();
			Server.Reset();
		}

		//When the ServerClient.cs gets a message, sends it here.
		//ServerClient.cs keeps local parity with server
		//Its information is always the most recent
		//It will contain the result of their move followed by mine.
		public void ServerInformation(String ServerMessage)
		{
			if (GameState != null)
			{
				Console.WriteLine("[ReversiGame_Server] " + ServerMessage);
				//Set player color
				if (ServerMessage.StartsWith("U") && ServerMessage.Length > 1)
				{
                    colorSet = true;
					int HumanColor = int.Parse(ServerMessage.Substring(1));
					GameState.Player1.Color = HumanColor == SERVER_BLACK ? Player.BLACK : Player.WHITE;
					GameState.Player2.Color = !GameState.Player1.Color;

					if (GameState.Player1.isAI)
					{
						GameState.Player1.AI.isWhite = GameState.Player1.Color == Player.WHITE;
					}

					currentTurn = HumanColor == SERVER_BLACK ? PLAYER1_TURN : PLAYER2_TURN;
					Globals.PostInfo("Color given by server: " + (GameState.Player1.Color == Player.WHITE ? "White" : "Black"));
				}

				//Game over. Server is done and no longer connected.
				if (ServerMessage.StartsWith("G"))
				{
					isPlaying = false;
					Globals.PostInfo("Game over received. Server connection closed.");
				}

				//Game results
				if (ServerMessage.StartsWith("W"))
				{
					isPlaying = false;
					GameState.winValue = int.Parse(ServerMessage.Substring(1)); ;
					Globals.PostInfo("Winner notification: " + GameState.winValue);
				}

				//Board Information
				if (ServerMessage.StartsWith("B"))
				{
					for (int i = 0; i < 64; i++)
					{
						int x = i / 8;
						int y = 7 - i % 8;
						int lastValue = BoardInformation[x, y];
						if (!int.TryParse(ServerMessage.Substring(i+1, 1), out BoardInformation[x, y]))
						{
							Console.WriteLine("Can't read board information. {0}={1},{2}=/={3}", i, x, y, ServerMessage.Substring(i+1, 1));
						}
					}

                    GameState.GameBoard = new OthelloBoard(BoardInformation);

					//Indicate we received a board from the server
					waitingMove = false;
				}

				//Server disconnect
				if (ServerMessage.StartsWith("E"))
				{
					isPlaying = false;
					Globals.PostInfo("Error. Game ended unexpectedly. Server disconnected.");
				}
			}
		}

		/* Use Worker.ReportProgress() to send data to the GUI */ 
		public bool Run(BackgroundWorker Worker)
		{
			int x = 0;
			int y = 0;
			isPlaying = true;
			string serverMessage;

			if (GameState == null)
			{
				return false;
			}

			Console.WriteLine("Game state: Running");

			if (GameState.isServerGame)
			{
				//Connect to the server
				Server.Connect();
				//Send up my username
				Server.SendMessage("N" + GameState.Player1.Name);
				//Read in my color
				serverMessage = Server.ReadMessage();
			}

			timer.Start();
			//Play the game!
			while (isPlaying)
			{
				if (Worker.CancellationPending)
				{
					isPlaying = false;
					return false;
				}

				GameState.ElapsedTime = timer.ElapsedMilliseconds;

				if (GameState.isServerGame)
				{
					//Do server play.
					//Read in the game board
					serverMessage = Server.ReadMessage();

					//I have a board. I should make a move
					if (serverMessage.StartsWith("B"))
					{
						//Report progress before we send our move up
						Worker.ReportProgress(20, GameState.Clone());
						if (GameState.Player1.isAI)
						{
							GameState.Player1.AI.UpdateBoard(GameState.GameBoard);
							GameState.Player1.AI.MakeMove(out x, out y);
						}
						else
						{
							GameState.CurrentPlayer = GameState.Player1;
							if (GameView.Communication == -1)
							{
								waitOnGui = false;
								Worker.ReportProgress(10, GameState.Clone());
								while (GameView.Communication < 1 && !Worker.CancellationPending) ;
								if (Worker.CancellationPending)
								{
									isPlaying = false;
									return false;
								}
								x = GameView.GUISubmitMove.X;
								y = GameView.GUISubmitMove.Y;
								Worker.ReportProgress(11, GameState.Clone());
								GameState.CurrentPlayer = GameState.Player2;
							}
							else
							{
								waitOnGui = true;
							}
						}
                        Server.SendMessage(string.Format("M{0}{1}", x, 7 - y));
					}
				}
				else
				{
					GameState.CurrentPlayer = (currentTurn == PLAYER1_TURN) ? GameState.Player1 : GameState.Player2;
					//Do local play
					Dictionary<int, Piece> playerMoves = GameState.GameBoard.PossibleMoves(GameState.CurrentPlayer.Color == Player.WHITE);
					//Skip my turn
					if (playerMoves.Count <= 0)
					{
						if (hadNoMoves)
						{
							isPlaying = false;
							if (GameState.GameBoard.WhitePieces.Count < GameState.GameBoard.BlackPieces.Count)
							{
								GameState.winValue = 0; // White/player loss
							}
							else if (GameState.GameBoard.WhitePieces.Count > GameState.GameBoard.BlackPieces.Count)
							{
								GameState.winValue = 1; //White/player win
							}
							else
							{
								GameState.winValue = 2;//Tie
							}
						}
						hadNoMoves = true;
						currentTurn = !currentTurn;
						continue;
					}
					if (GameState.CurrentPlayer.isAI)
					{
						GameState.CurrentPlayer.AI.isWhite = GameState.CurrentPlayer.Color == Player.WHITE;
						GameState.CurrentPlayer.AI.UpdateBoard(GameState.GameBoard);
						GameState.CurrentPlayer.AI.MakeMove(out x, out y);
					}
					else
					{
						//Capture gui
						if (GameView.Communication == -1)
						{
							waitOnGui = false;

							//Game view is open to capture a game point
							Worker.ReportProgress(10, GameState.Clone());

							//Hold until the value of the move is set.
							while (GameView.Communication < 1 && !Worker.CancellationPending) ;

							if (Worker.CancellationPending)
							{
								isPlaying = false;
								return false;
							}

							x = GameView.GUISubmitMove.X;
							y = GameView.GUISubmitMove.Y;

							//Tell the GUI I've read the move
							Worker.ReportProgress(11, GameState.Clone());
						}
						else
						{
							waitOnGui = true;
						}
					}

					//Update new move
					if (GameState != null && !waitOnGui)
					{
						if (x == -1 || y == -1)
						{
							if (LastMove.x == -1 && LastMove.y == -1)
							{
								isPlaying = false;
							}
							else
							{
								LastMove.x = -1;
								LastMove.y = -1;
							}
						}
						else
						{
							GameState.GameBoard.MakeMove(x, y, GameState.CurrentPlayer.Color == Player.WHITE);
							LastMove = GameState.GameBoard.GetPiece(x, y);
							GameState.turn = GameState.turn + 1;

							//Add a new piece representing this move.
							GameState.GameMoves.Add(new Piece(x, y, GameState.CurrentPlayer.Color == Player.WHITE ? ServerClient.WHITE_PIECE : ServerClient.BLACK_PIECE));
							
							Worker.ReportProgress(0, GameState.Clone());
						}

						currentTurn = !currentTurn;
					}
				}
			}

			Worker.ReportProgress(100, GameState.Clone());

			return true;
		}

		public void Configure(GameData Data)
		{
			Reset();

			//Clone player info in
			GameState.Player1.Clone(Data.Player1);
			GameState.Player2.Clone(Data.Player2);

			//Reset game board
			GameState.GameBoard = Data.GameBoard.Clone();

			//Server Configuration
			if (Data.isServerGame)
			{
				int serverPort = 0;
				int.TryParse(Data.ServerPort, out serverPort); // False on error. Check formatting.

				Server.Port = serverPort;
				Server.ServerIP = Data.ServerIP;
				Server.Name = GameState.Player1.Name;
				GameState.Player1.Name = Server.Name; // This is because the server might do name fixing of some sort.
			}

			currentTurn = GameState.Player1.Color == Player.WHITE ? PLAYER2_TURN : PLAYER1_TURN;
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OthelloAI
{
	/* A bitboard representing an othello board state
	 * Uses 2 64-bit integers to represent the black/white tiles
	 * Supports construction from a string or int[,]
	 */

	public class Piece
	{
		public int x;
		public int y;
		public byte cell;

		public Piece(int x, int y, byte cellValue)
		{
			this.x = x;
			this.y = y;
			this.cell = cellValue;
		}

		public Piece Clone()
		{
			return new Piece(x, y, cell);
		}

		public override string ToString()
		{
			return string.Format("({0}, {1}, {2})", x, y, cell);
		}
	}

	public class OthelloBoard
	{
		public Dictionary<int, Piece> WhitePieces;
		public Dictionary<int, Piece> BlackPieces;
		public List<Tuple<int, int>> Perimeter;

		//Knwon that (x, y) are in [0, 7], and |x|*|y| = 64
		public static int XYToInt(int x, int y)
		{
			return x * 8 + y;
		}

		//Tuple<x, y>
		public static Tuple<int, int> IntToXY(int i)
		{
			return Tuple.Create<int, int>(i / 8, i % 8);
		}

		//Starts with a fresh othello board
		public OthelloBoard()
		{
			WhitePieces = new Dictionary<int, Piece>();
			BlackPieces = new Dictionary<int, Piece>();
			Perimeter = new List<Tuple<int, int>>();

			//Build a basic board
			for (int x = 2; x < 6; x++)
			{
				for (int y = 2; y < 6; y++)
				{
					if (x >= 3 && x <= 4 && y >= 3 && y <= 4)
					{
						if (x == y)
						{
							WhitePieces[XYToInt(x, y)] = new Piece(x, y, ServerClient.WHITE_PIECE);
						}
						else
						{
							BlackPieces[XYToInt(x, y)] = new Piece(x, y, ServerClient.BLACK_PIECE);
						}
					}
					else
					{
						Perimeter.Add(Tuple.Create<int, int>(x, y));
					}
				}
			}
		}

        public OthelloBoard(int[,] BoardInformation)
        {
            int x;
            int y;
            WhitePieces = new Dictionary<int, Piece>();
            BlackPieces = new Dictionary<int, Piece>();
            Perimeter = new List<Tuple<int, int>>();    
            // PLACE PIECES
            for(x = 0; x < 8; x++)
            {
                for(y = 0; y < 8; y++)
                {
                    if (BoardInformation[x, y] == ServerClient.EMPTY_PIECE)
                    {
                        // Nothing here!
                    }
                    else
                    {
                        if (BoardInformation[x, y] == ServerClient.WHITE_PIECE)
                        {
                            WhitePieces[XYToInt(x, y)] = new Piece(x, y, ServerClient.WHITE_PIECE);
                        }
                        if (BoardInformation[x, y] == ServerClient.BLACK_PIECE)
                        {
                            BlackPieces[XYToInt(x, y)] = new Piece(x, y, ServerClient.BLACK_PIECE);
                        }
                    }
                }
            }

            int i;
            int j;
            bool IsFence;
            // CREATE PERIMETER
            for(x = 0; x < 8; x++)
            {
                for(y = 0; y < 8; y++)
                {
                    if (!WhitePieces.ContainsKey(XYToInt(x, y)) && !BlackPieces.ContainsKey(XYToInt(x, y)))
                    {
						IsFence = false;
						for (i = -1; i <= 1 && !IsFence; i++)
                        {
							for (j = -1; j <= 1 && !IsFence; j++)
                            {
                                if ((i == 0 && j == 0) || x + i < 0 || x + i > 7 || y + j < 0 || y + j > 7)
                                {
                                    // Out of Bounds, Stop Check
                                }
                                else
                                {
                                    if (WhitePieces.ContainsKey(XYToInt(x + i, y + j)) || BlackPieces.ContainsKey(XYToInt(x + i, y + j)))
                                    {
                                        IsFence = true;
                                    }
                                }
                            }
                        }

						if (IsFence)
						{
							Perimeter.Add(Tuple.Create<int, int>(x, y));
						}
                    }
                }
            }
        }

		public OthelloBoard Clone()
		{
			OthelloBoard temp = new OthelloBoard();
			temp.WhitePieces.Clear();
			temp.BlackPieces.Clear();
			temp.Perimeter.Clear();
			foreach (KeyValuePair<int, Piece> white in WhitePieces)
			{
				temp.WhitePieces.Add(white.Key, white.Value);
			}
			foreach (KeyValuePair<int, Piece> black in BlackPieces)
			{
				temp.BlackPieces.Add(black.Key, black.Value);
			}
			foreach (Tuple<int, int> value in Perimeter)
			{
				temp.Perimeter.Add(value);
			}
			return temp;
		}

        //Attempts to make a move.
        //Will cahnge the board with the flip effects
        //Returns the list of turned pieces, or null if the move for that player is invalid.
        public List<Piece> MakeMove(int x, int y, bool ForWhite)
		{
			List<Piece> pieces = MoveEffects(x, y, ForWhite);
			
			int map = XYToInt(x, y);
			
			byte pieceValue = ForWhite ? ServerClient.WHITE_PIECE : ServerClient.BLACK_PIECE;

			Dictionary<int, Piece> CurrentColor = ForWhite ? WhitePieces : BlackPieces;
			Dictionary<int, Piece> Opposition = ForWhite ? BlackPieces : WhitePieces;
			
			if (pieces != null)
			{
				//First item in the list is the new one!
				CurrentColor[map] = pieces[0];
				pieces.RemoveAt(0);
				foreach (Piece oppositionPiece in pieces)
				{
					int pieceMap = XYToInt(oppositionPiece.x, oppositionPiece.y);
					Opposition.Remove(pieceMap);
					oppositionPiece.cell = pieceValue;
					if (CurrentColor.ContainsKey(pieceMap))
					{
						//Error
						Console.WriteLine("["+pieceMap+"]");
					}
					else
					{
						CurrentColor.Add(pieceMap, oppositionPiece);
					}
				}

				//Update the perimeter
				Perimeter.Remove(Tuple.Create<int, int>(x, y));
				for (int i = -1; i <= 1; i++)
				{
					for (int j = -1; j <= 1; j++)
					{
						if (i == 0 && j == 0) continue;
						if (!Perimeter.Contains(Tuple.Create<int, int>(x + i, y + j)) && !Opposition.ContainsKey(XYToInt(x + i, y + j)) && !CurrentColor.ContainsKey(XYToInt(x + i, y + j)) && (x + i) < 8 && (y + j) < 8 && (x + i) >= 0 && (y + j) >= 0)
						{
							Perimeter.Add(Tuple.Create<int, int>(x + i, y + j));
						}
					}
				}
			}

			return pieces;
		}

		//Returns a list of the pieces turned by the move, or null if it's not valid
		//Includes the potential new piece
		public List<Piece> MoveEffects(int x, int y, bool ForWhite)
		{
			int tx, ty, map;

			List<Piece> outList = null;
			List<Piece> turnedPieces = new List<Piece>();

			Dictionary<int, Piece> CurrentColor = ForWhite ? WhitePieces : BlackPieces;
			Dictionary<int, Piece> Opposition = ForWhite ? BlackPieces : WhitePieces;

			//Skip out if this move isn't on the perimeter.
			if (Perimeter.Contains(Tuple.Create<int, int>(x, y)) != true)
			{
				return null;
			}

			for (int deltaX = -1; deltaX <= 1; deltaX++)
			{
				turnedPieces.Clear();
				for (int deltaY = -1; deltaY <= 1; deltaY++)
				{
					if (deltaX == 0 && deltaY == 0) continue;

					tx = x + deltaX;
					ty = y + deltaY;
					if (tx < 0 || ty < 0 || tx > 7 || ty > 7)
					{
						continue;
					}
					map = XYToInt(tx, ty);

					//Cache a line of opposition
					while (Opposition.ContainsKey(map))
					{
						turnedPieces.Add(Opposition[map]);

						tx += deltaX;
						ty += deltaY;

						if (tx < 0 || ty < 0 || tx > 7 || ty > 7)
						{
							break;
						}
						map = XYToInt(tx, ty);
					}

					//If this move will cap off, merge
					if (turnedPieces.Count > 0)
					{
						if (CurrentColor.ContainsKey(map))
						{
							if (outList == null)
							{
								outList = new List<Piece>();
								outList.Add(new Piece(x, y, ForWhite ? ServerClient.WHITE_PIECE : ServerClient.BLACK_PIECE));
							}
							outList.AddRange(turnedPieces);
						}
						turnedPieces.Clear();
					}
				}
			}

			return outList;
		}

		//Value of "cell" is the number of flipped opposition tiles if current player makes that move.
		public Dictionary<int, Piece> PossibleMoves(bool ForWhite)
		{
			Dictionary<int, Piece> moveSet = new Dictionary<int, Piece>();

			//Scan the perimeter and try each direction
			foreach (Tuple<int, int> square in Perimeter)
			{
				List<Piece> moveResult = MoveEffects(square.Item1, square.Item2, ForWhite);
				if (moveResult != null)
				{
					moveSet[XYToInt(square.Item1, square.Item2)] = new Piece(square.Item1, square.Item2, (byte) moveResult.Count);
				}
			}

			return moveSet;
		}

		public Piece GetPiece(int x, int y)
		{
			Piece returnPiece = null;

			int map = XYToInt(x, y);
			if (BlackPieces.ContainsKey(map))
			{
				returnPiece = BlackPieces[map];
			}
			else if (WhitePieces.ContainsKey(map))
			{
				returnPiece = WhitePieces[map];
			}

			return returnPiece;
		}

		public override string ToString()
		{
			int position;
			StringBuilder strView = new StringBuilder();
			strView.Append('-', 9);
			strView.Append("\n ");
			for (int i = 0; i < 8; i++) strView.Append(i);
			strView.Append('\n');
			for (int y = 0; y < 8; y++)
			{
				strView.Append(y);
				for (int x = 0; x < 8; x++)
				{
					position = XYToInt(x, y);
					bool b = false;
					if (WhitePieces.ContainsKey(position))
					{
						strView.Append("W");
						b = true;
					}
					if (BlackPieces.ContainsKey(position))
					{
						strView.Append("B");
						b = true;
					}

					if (Perimeter.Contains(Tuple.Create<int, int>(x, y)))
					{
						strView.Append("+");
						b = true;
					}
					else
					{
						if (!b) strView.Append(".");
					}
				}
				strView.Append('\n');
			}
			strView.Append('-', 9);

			return strView.ToString();
		}
	}
}

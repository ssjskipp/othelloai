﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OthelloAI
{

	public delegate void InfoPost(string information);

	public class Globals
	{
		//Non-player options: "Server"
		public const string ServerType = "Server";
		public static List<string> PlayerOptionsEnum = new List<string>{"Human Player", "Naive AI", "AILvl01", "DantAI", "Mover"};
		public static Random RNG = new Random();

		//Delegated function to post string data.
		public static InfoPost PostInfo;

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OthelloAI
{
	public class GameData
	{

		//Possible player types are Globals.PlayerOptionsEnum and "Server"
		public Player Player1 = new Player();
		public Player Player2 = new Player();

		public OthelloBoard GameBoard = new OthelloBoard();

		public int winValue = -1;
		
		public long ElapsedTime = 0;

		public Player CurrentPlayer;
		public int turn;

		//This is filled from the game
		//In the order of valid moves.
		public List<Piece> GameMoves = new List<Piece>();

		//Define these if a player is ServerType
		public string ServerIP;
		public string ServerPort;

		public bool isServerGame
		{
			get
			{
				return Player2.Type == Globals.ServerType;
			}
		}

		public GameData()
		{
		}

		public void Reset()
		{
			ElapsedTime = 0;
			turn = 0;
			winValue = -1;
			CurrentPlayer = null;
			GameBoard = new OthelloBoard();
			GameMoves.Clear();
		}

		//A clone of this game data object
		public GameData Clone()
		{
			GameData newGameData = new GameData();
			newGameData.ServerIP = ServerIP;
			newGameData.ServerPort = ServerPort;
			newGameData.turn = turn;
			newGameData.CurrentPlayer = CurrentPlayer;
			newGameData.ElapsedTime = ElapsedTime;

			//Different pattern since these exist as allocated classes already
			newGameData.Player1.Clone(Player1);
			newGameData.Player2.Clone(Player2);

			//Shouldn't clone from move list
			//But should clone move list over
			newGameData.GameBoard = GameBoard.Clone();
			for (int i = 0; i < GameMoves.Count; i++)
			{
				//newGameData.GameBoard.MakeMove(GameMoves[i].x, GameMoves[i].y, GameMoves[i].cell == ServerClient.WHITE_PIECE);
				newGameData.GameMoves.Add(GameMoves[i].Clone());
			}

			return newGameData;
		}

		//Need to be able to update this game given a game board
		//This is used as a delta between two moves
		//Same as AI update board
		public void UpdateGameBoard(OthelloBoard NewBoard)
		{

		}

		public override string ToString()
		{
			return string.Format("P1: {0}, P2: {1}", Player1.ToString(), Player2.ToString());
		}
	}
}

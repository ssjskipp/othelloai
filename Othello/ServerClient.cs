﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;
using System.Net;
using System.Net.Sockets;

/* Code copied (and modified/integrated) From
 *										 Reversi client
 *										 by Cheng Chen
 *
 * This source file by Chris Federici, Lorenzo Grompone, and Paul DiCola
 * 
 * Converted to be async
 */ 

namespace OthelloAI
{
	public delegate void ServerMessage(string ServerMessage);
	public delegate void MakeMove(ServerClient TargetClient);
	public delegate void LogOutput(string message);

	public class ServerClient
	{
		public const byte BLACK_PIECE = 2;
		public const byte WHITE_PIECE = 1;
		public const int EMPTY_PIECE = 0;

		//Delegate Call (Events)
		public ServerMessage OnMessage;
		public MakeMove OnMakeMove;
		public LogOutput OnLog;

		//Server Information
		int _port = 4000;
		public int Port
		{
			get { return _port; }
			set
			{
				_port = Port;
			}
		}
		IPAddress _serverAddress = IPAddress.Parse("127.0.0.1");
		public string ServerIP
		{
			get
			{
				return _serverAddress.ToString();
			}
			set
			{
				if (!IPAddress.TryParse(value, out _serverAddress))
				{
					_serverAddress = IPAddress.Parse("127.0.0.1");
				}
			}
		}
		string _name = "Razzmatazz";
		public string Name
		{
			get { return _name; }
			set
			{
				_name = Regex.IsMatch(value, @"[a-zA-Z0-9_]") ? value : "Razzmatazz";
				if (_name.Length > 10)
				{
					_name = _name.Substring(0, 10);
				}
			}
		}

		//Bytes to read a message into
		byte[] recvBytes = new byte[128];

		//Game State Information
		public int ServerColor { get; private set; }
		public bool IsGameOver { get; private set; }

		//the winning information server sent. 0 lose, 1 win, 2 tie. You may customize reaction if AI win :)
		public int Winner { get; private set; }

		//the x,y position of the next move. Valid values are [0,7].
		public int MoveXIndex = 0;
		public int MoveYIndex = 0;

		//The last move sent to the server
		public int LastMoveX = -1;
		public int LastMoveY = -1;

		//The socket connectio
		Socket ServerSocket;

		//Defaults are local
		public ServerClient(string Nickname = "Razzmatazz", int ServerPort = 4000, string ServerIP = "127.0.0.1")
		{
			this.Name = Nickname;
			this.Port = ServerPort;
			this.ServerIP = ServerIP;

			OnLog = new LogOutput(log);
		}

        //Temporary process to provide delegate functions
        public ServerClient(MakeMove makeMove, LogOutput logOutput, string Nickname = "Razzmatazz", int ServerPort = 4000, string ServerIP = "127.0.0.1")
        {
            this.Name = Nickname;
            this.Port = ServerPort;
            this.ServerIP = ServerIP;

            this.OnLog += logOutput;
            this.OnMakeMove += makeMove;

			if (OnLog != null) OnLog(this.ToString());
        }

		public bool isConnected()
		{
			if (ServerSocket == null || !ServerSocket.Connected)
				return false;
			return true;
		}

		//TODO: Clean up server socket
		public void Reset()
		{
			if (ServerSocket != null)
			{
				if (ServerSocket.Connected)
				{
					ServerSocket.Disconnect(false);
				}
				ServerSocket.Dispose();
			}
		}

		void log(string str)
		{
			Console.WriteLine("[ServerLog] " + str);
		}

		//Start a connection to the server
		//Begins the chain of action
		public bool Connect()
		{
			bool exitStatus;
			IPEndPoint ServerEndpoint;
			// Socket GameSocket;

			ServerEndpoint = new IPEndPoint(_serverAddress, _port);
			ServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			exitStatus = true;

			try
			{
				OnLog("Client " + Name + " connecting: (" + _port + ", " + ServerIP + ")");
				ServerSocket.Connect(ServerEndpoint);
			}
			catch (SocketException e)
			{
				OnLog("socketException: " + e);
				OnMessage("LError. Could not connect to server. Check configuration.");
				exitStatus = false;
			}
			return exitStatus;
		}

		//TODO: Handle socket exception here
		//Implement exit gracefully
		public string ReadMessage()
		{
			string messageString = "";
			try
			{
				int numBytes = ServerSocket.Receive(recvBytes, 0, recvBytes.Length, SocketFlags.None);
				if (numBytes > 0)
				{
					messageString = Encoding.ASCII.GetString(recvBytes, 0, numBytes);
					OnMessage(messageString);
				}
			}
			catch (SocketException e)
			{
				messageString = "E";
				OnMessage("E");
				ServerSocket.Dispose();
			}
			return messageString;
		}

		//Sends a message, then listens for a response
		public void SendMessage(string message)
		{
			OnLog("Sending: " + message);
			byte[] sendBytes = Encoding.ASCII.GetBytes(message + "\n");
			ServerSocket.Send(sendBytes, sendBytes.Length, SocketFlags.None);
		}

		public override string ToString()
		{
			return String.Format("[Client,(Name={0},IP={1},Port={2})]", Name, ServerIP, Port);
		}
    }
}
